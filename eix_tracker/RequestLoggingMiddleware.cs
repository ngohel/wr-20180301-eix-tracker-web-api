﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace eix_tracker
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;

        public MySqlConnection connection()
        {
            //string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            return new MySqlConnection(Myconectionstring);
        }

        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var injectedRequestStream = new MemoryStream();
            try
            {
                var requestLog = $"REQUEST HttpMethod: {context.Request.Method}, Path: {context.Request.Path}";
                var bodyAsText = "";
                using (var bodyReader = new StreamReader(context.Request.Body))
                {
                    bodyAsText = bodyReader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(bodyAsText) == false)
                    {
                        requestLog += $", Body : {bodyAsText}";
                    }
                    var bytesToWrite = Encoding.UTF8.GetBytes(bodyAsText);
                    injectedRequestStream.Write(bytesToWrite, 0, bytesToWrite.Length);
                    injectedRequestStream.Seek(0, SeekOrigin.Begin);
                    context.Request.Body = injectedRequestStream;
                }
                if (bodyAsText != "")
                {
                    using (MySqlConnection conn = connection())
                    {
                        conn.Open();
                        MySqlCommand cmd = new MySqlCommand("sms_add_api_req", conn);
                        DataSet ds = new DataSet();
                        using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                        {
                            //for single recored from json
                            //var obj = JObject.Parse(bodyAsText);
                            //var naem = obj["username"].ToString();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@user_id", 1);
                            cmd.Parameters.AddWithValue("@api_name", context.Request.Path);
                            cmd.Parameters.AddWithValue("@api_method", context.Request.Method);
                            cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                            cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                            cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                            cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                            adp.Fill(ds);
                            //cmd.ExecuteNonQuery();
                        }
                    }
                }
                _logger.LogTrace(requestLog);
                await _next.Invoke(context);
            }
            finally
            {
                injectedRequestStream.Dispose();
            }
        }
    }
}
