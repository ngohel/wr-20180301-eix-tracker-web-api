﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Services;
using eix_tracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Task")]
    public class TaskController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<TaskController> _logger;
        public TaskController(IConfiguration config, ILogger<TaskController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [Authorize]
        [HttpPost("Add_user_Task")]
        public JsonResult Add_user_Task([FromBody]TaskManage tk)
        {
            try
            {
                taskservices tks = new taskservices(_config);
                var response = tks.add_user_task(tk);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("View_user_task")]
        [ResponseCache(VaryByHeader = "View_user_task", Duration = 100)]
        public JsonResult View_user_task(TaskManage tm)
        {
            try
            {
                taskservices tks = new taskservices(_config);
                var response = tks.View_user_task(tm);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("View_task_Details")]
        //[ResponseCache(VaryByHeader = "View_task_Details", Duration = 100)]
        public JsonResult View_task_details(TaskManage tm)
        {
            try
            {
                taskservices tks = new taskservices(_config);
                var response = tks.View_task_details(tm);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPut("update_user_task")]
        public JsonResult update_user_task([FromBody]TaskManage tm)
        {
            try
            {
                taskservices tks = new taskservices(_config);
                var response = tks.update_user_task(tm);
                return Json(response);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpDelete("delete_user_task")]
        public JsonResult delete_user_task([FromBody]TaskManage tm)
        {
            try
            {
                taskservices tsk = new taskservices(_config);
                var response = tsk.delete_task_details(tm);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //for Android delete same as delete_user_task
        [Authorize]
        [HttpDelete("delete_user_task_andrd")]
        public JsonResult delete_user_task_andrd(TaskManage tm)
        {
            try
            {
                taskservices tsk = new taskservices(_config);
                var response = tsk.delete_task_details(tm);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("location_based_services")]
        public JsonResult location_based_services(int location_id)
        {
            try
            {
                taskservices tsk = new taskservices(_config);
                var response = tsk.sms_location_based_services(location_id);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("services_based_questions")]
        public JsonResult services_based_questions(int service_id)
        {
            try
            {
                taskservices tsk = new taskservices(_config);
                var response = tsk.sms_services_based_questions(service_id);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("view_services_detail")]
        public JsonResult sms_view_services_detail(int servicesid)
        {
            try
            {
                taskservices tsk = new taskservices(_config);
                var response = tsk.sms_services_based_questions(servicesid);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }


    }
}
