﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Company")]
    public class CompanyController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<CompanyController> _logger;
        public CompanyController(IConfiguration config,ILogger<CompanyController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [Authorize]
        [HttpGet("displaycompany")]
        public JsonResult Get_company()
        {
            try
            {
                CompanyService cs = new CompanyService(_config);
                var response = cs.display_company();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("add_service_question")]
        public JsonResult sms_add_service_question([FromBody]sms_add_service_question adsq)
        {
            try
            {
                CompanyService cs = new CompanyService(_config);
                var response = cs.sms_add_service_question(adsq);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("view_service_question")]
        public JsonResult view_service_question(int userid, int roleid)
        {
            try
            {
                CompanyService cs = new CompanyService(_config);
                var response = cs.sms_view_service_question(userid, roleid);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPut("update_service_question")]
        public JsonResult sms_update_service_question([FromBody]sms_add_service_question adsq)
        {
            try
            {
                CompanyService cs = new CompanyService(_config);
                var response = cs.sms_add_service_question(adsq);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpDelete("delete_service_question")]
        public JsonResult sms_delete_service_question([FromBody]sms_add_service_question adsq)
        {
            try
            {
                CompanyService cs = new CompanyService(_config);
                var response = cs.sms_delete_service_question(adsq);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
