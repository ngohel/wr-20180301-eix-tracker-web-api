﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/attachment")]
    public class attachmentController : Controller
    {
        private readonly ILogger<attachmentController> _logger;
        private IConfiguration Configuration { get; set; }
        public attachmentController(ILogger<attachmentController> logger, IConfiguration configuration)
        {
            _logger = logger;
            Configuration = configuration;
        }

        [Authorize]
        [HttpPost("add_attachment")]
        public JsonResult user_add_attachment([FromBody]attachmentCollection atcollection)
        {
            try
            {
                attachmentServices ats = new attachmentServices(Configuration);
                var response = ats.add_attachments(atcollection);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("view_attachments")]
        public JsonResult view_attachments(attachment attach)
        {
            try
            {
                attachmentServices atch = new attachmentServices(Configuration);
                var response = atch.view_attachments(attach);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}