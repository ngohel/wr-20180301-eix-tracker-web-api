﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Client")]
    public class ClientController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<ClientController> _logger;
        public ClientController(IConfiguration config, ILogger<ClientController> logger)
        {
            _config = config;
            _logger = logger;

        }

        [Authorize]
        [HttpPost("Add_Client_location")]
        public JsonResult Add_client_location([FromBody]Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.add_client_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("client_based_location")]
        public JsonResult client_based_location(Client cl)
        {
            ClientServices cs = new ClientServices(_config);
            var response = cs.client_based_location(cl);
            return Json(response);
        }

        [Authorize]
        [HttpGet("client_total_location")]
        [ResponseCache(VaryByHeader = "client_total_location", Duration = 100)]
        public JsonResult client_total_location(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.client_total_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_view_location")]
        [ResponseCache(VaryByHeader = "user_view_location", Duration = 100)]
        public JsonResult User_view_location(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.view_user_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }

        [Authorize]
        [HttpGet("user_location_detail")]
        //[ResponseCache(VaryByHeader = "user_location_detail", Duration = 100)]
        public JsonResult user_location_detail(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.user_Location_Details(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("user_location_update")]
        public JsonResult user_location_Location([FromBody]Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.update_user_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpDelete("user_location_delete")]
        public JsonResult user_location_delete(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.Delete_User_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}