﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/ErrorController")]
    public class ErrorControllerController : Controller
    {

        [HttpGet]
        [Route("")]
        public IActionResult ServerError()
        {

            var feature = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            var content = new ExceptionMessageContent()
            {
                Error = "Unexpected Server Error",
                Message = feature?.Error.Message
            };
            return Content(JsonConvert.SerializeObject(content), "application/json");
        }

        [HttpGet]
        [Route("{statusCode}")]
        public IActionResult StatusCodeError(int statusCode)
        {

            var feature = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            var content = new ExceptionMessageContent() { Error = "Server Error", Message = $"The Server responded with status code {statusCode}" };
            return Content(JsonConvert.SerializeObject(content), "application/json");

        }
    }
}