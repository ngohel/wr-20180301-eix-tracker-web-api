﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;
using System;
using System.Collections.Generic;
using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Data;
using Microsoft.Extensions.Logging;
using System.IO;
using Rotativa.AspNetCore.Options;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Demo")]
    public class DemoController : Controller
    {
        //public string ConnectionStrings { get; set; }
        public MySqlConnection connection()
        {
            //string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            return new MySqlConnection(Myconectionstring);
        }
        public IActionResult DemoViewAsPDF()
        {
            return new ViewAsPdf("DemoViewAsPDF");
        }

        //public IActionResult DemoPageMarginsPDF()
        //{
        //    var report = new ViewAsPdf("DemoPageMarginsPDF")
        //    {
        //        PageMargins = { Left = 20, Bottom = 20, Right = 20, Top = 20 },
        //    };
        //    return report;
        //}

        //public IActionResult DemoOrientationPDF(string Orientation)
        //{
        //    if (Orientation == "Portrait")
        //    {
        //        var demoViewPortrait = new ViewAsPdf("DemoOrientationPDF")
        //        {
        //            PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
        //        };
        //        return demoViewPortrait;
        //    }
        //    else
        //    {
        //        var demoViewLandscape = new ViewAsPdf("DemoOrientationPDF")
        //        {
        //            PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
        //        };
        //        return demoViewLandscape;
        //    }
        //}

        //public IActionResult DemoPageNumberPDF()
        //{
        //    return new ViewAsPdf("DemoPageNumberPDF")
        //    {
        //        CustomSwitches = "--page-offset 0 --footer-center [page] --footer-font-size 12"
        //    };
        //}


        //public IActionResult DemoPageSizePDF()
        //{
        //    return new Rotativa.AspNetCore.ViewAsPdf("DemoPageSizePDF")
        //    {
        //        PageSize = Rotativa.AspNetCore.Options.Size.A6,
        //    };
        //}

        //public IActionResult DemoPageNumberwithCurrentDate()
        //{
        //    var pdfResult = new ViewAsPdf("DemoPageNumberwithCurrentDate")
        //    {
        //        CustomSwitches =
        //            "--footer-center \"  Created Date: " +
        //            DateTime.Now.Date.ToString("dd/MM/yyyy") + "  Page: [page]/[toPage]\"" +
        //            " --footer-line --footer-font-size \"12\" --footer-spacing 1 --footer-font-name \"Segoe UI\""
        //    };

        //    return pdfResult;
        //}
        [HttpGet("role_list_new")]
        public IActionResult DemoModelPDF()
        {
            //List<Customer> customerList = new List<Customer>()
            //{
            //    new Customer { CustomerID = 1, Address = "Taj Lands Ends 1", City = "Mumbai" , Country ="India", Name ="Sai", Phoneno ="9000000000"},
            //    new Customer { CustomerID = 2, Address = "Taj Lands Ends 2", City = "Mumbai" , Country ="India", Name ="Ram", Phoneno ="9000000000"},
            //    new Customer { CustomerID = 3, Address = "Taj Lands Ends 3", City = "Mumbai" , Country ="India", Name ="Sainesh", Phoneno ="9000000000"},
            //    new Customer { CustomerID = 4, Address = "Taj Lands Ends 4", City = "Mumbai" , Country ="India", Name ="Saineshwar", Phoneno ="9000000000"},
            //    new Customer { CustomerID = 5, Address = "Taj Lands Ends 5", City = "Mumbai" , Country ="India", Name ="Saibags", Phoneno ="9000000000"}
            //};
            //string Myconectionstring = "server=localhost;database=trackerdb;user=root;password=";
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Role()
                        {
                            Role_Id = Convert.ToInt16(reader["Role_ID"]).ToString(),
                            Role_Name = reader["Role_Name"].ToString(),
                        });
                    }
                }
            }
            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/pdf/"), uniqueFileName + ".pdf");
            ViewBag.Message = string.Format("Hello {0} to ASP.NET MVC!", "Giorgio III.");

            return new ViewAsPdf("DemoModelPDF", list)
            {
                FileName = uniqueFileName,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageMargins = { Left = 0, Right = 0 },
                SaveOnServerPath = filePath,
            };
            //return new ViewAsPdf("DemoModelPDF", filePath);

            //return new ViewAsPdf("DemoModelPDF", list);
            //return RedirectToAction("Download");
        }


        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, Path.GetFileName(path));
        }
    }
}
