﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Logins")]
    public class LoginsController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<LoginsController> _logger;
        public LoginsController(IConfiguration config,ILogger<LoginsController> logger)
        {
            _config = config;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();

        }

        [HttpGet("Login_Attempt")]
        public JsonResult Login_Attempt(Login_Attempt lga)
        {
            try
            {
                LoginServices ls = new LoginServices(_config);
                var data = ls.Login_Attempt(lga);
                return Json(data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[AllowAnonymous]
        //[HttpPost("test")]
        //public JsonResult Getlogin([FromForm]string uname, string pswd)
        //{
        //    LoginServices ls = new LoginServices();
        //    var datra = ls.Getloginuser(uname, pswd);
        //    return Json(datra);
        //    //if (datra == true)
        //    //{
        //    //    return 1;
        //    //}
        //    //else
        //    //{
        //    //    return 0;
        //    //}
        //}

        //[AllowAnonymous]
        //[HttpPost]
        //public IActionResult RequestToken([FromBody] string uname, string pswd)
        //{

        //    if (uname == "Jon" && pswd == "Again, not for production use, DEMO ONLY!")
        //    {
        //        var claims = new[]
        //        {
        //            new Claim(ClaimTypes.Name, uname)
        //        };

        //        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["SecurityKey"]));
        //        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        //        var token = new JwtSecurityToken(
        //            issuer: "yourdomain.com",
        //            audience: "yourdomain.com",
        //            claims: claims,
        //            expires: DateTime.Now.AddMinutes(30),
        //            signingCredentials: creds);

        //        return Ok(new
        //        {
        //            token = new JwtSecurityTokenHandler().WriteToken(token)
        //        });
        //    }

        //    return BadRequest("Could not verify username and password");

        //}

    }
}