﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Rotativa.AspNetCore;
using System.IO;
using Rotativa.AspNetCore.Options;
using MySql.Data.MySqlClient;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Report")]
    public class ReportController : Controller
    {
        private readonly ILogger<ReportController> _logger;
        private IConfiguration _config;
        public ReportController(IConfiguration config, ILogger<ReportController> logger)
        {
            _config = config;
            _logger = logger;
        }
        #region connection
        public MySqlConnection connection()
        {
            string Myconectionstring = this._config.GetValue<string>("ConnectionStrings:DefaultConnection");
            return new MySqlConnection(Myconectionstring);
        }
        #endregion

        [Authorize]
        [HttpPost("Add_user_report")]
        public JsonResult Insert_Report([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.Insert_Report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }


        [Authorize]
        [HttpPost("Add_user_report_detail")]
        public JsonResult Insert_Report_detail([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.Insert_Report_detail(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_View_Report")]
        [ResponseCache(VaryByHeader = "user_View_Report", Duration = 100)]
        public JsonResult user_View_Report(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_view_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("view_incident_report")]
        public JsonResult view_incident_report(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.view_incident_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_report_detail")]
        //[ResponseCache(VaryByHeader = "user_report_detail", Duration = 100)]
        public JsonResult user_view_report(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_report_detail(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [HttpPost]
        public FileResult Export(string GridHtml)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(GridHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Grid.pdf");
            }
        }


        public string Index()
        {
            PdfReader reader = new PdfReader("template.pdf");
            return "SomeText";
        }

        [Authorize]
        [HttpDelete("user_report_delete")]
        public JsonResult user_delete_report([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_delete_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        #region Changes For Delete Method Without body Android
        //for ajay android change new one created
        [Authorize]
        [HttpDelete("user_report_delete_andrd")]
        public JsonResult user_delete_report_andrd(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_delete_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //for ajay android change new one created
        [Authorize]
        [HttpDelete("user_report_delete_detail_andrd")]
        public JsonResult user_delete_report_detail_andrd(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_delete_report_detail(rpt);
                return Json(response);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
        #endregion

        [Authorize]
        [HttpDelete("user_report_delete_detail")]
        public JsonResult user_delete_report_detail([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_delete_report_detail(rpt);
                return Json(response);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPut("user_report_update")]
        public JsonResult user_report_update([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.user_report_update(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [HttpGet("get_report_detail_pdf")]
        public JsonResult pdf(Report_detail_pdf rg)
        {
            try
            {
                MemoryStream workStream = new MemoryStream();
                Document document = new Document();
                PdfWriter.GetInstance(document, workStream).CloseStream = false;


                List<Report_detail_pdf> list = new List<Report_detail_pdf>();
                using (MySqlConnection conn = connection())
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("sms_view_report_detail", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@reportid", rg.report_id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Report_detail_pdf()
                            {
                                firstname = reader["first_name"].ToString() + " " + reader["last_name"].ToString(),
                                address_one = reader["address_one"].ToString(),
                                address_two = reader["address_two"].ToString(),
                                report_desc = reader["report_desc"].ToString(),
                                comp_name = reader["comp_name"].ToString(),
                                user_added_by_role_name = reader["user_added_by_role_name"].ToString(),
                                security_company_addressone = reader["security_company_address_one"].ToString(),
                                security_company_mobile = reader["security_company_phone_no"].ToString(),
                                report_type = reader["report_type"].ToString(),
                                location = reader["address_one"].ToString() + " " + reader["location_name"].ToString(),
                                user_by = reader["user_added_by_firstname"].ToString() + " " + reader["user_added_by_lastname"].ToString(),
                                report_date = reader["report_date"].ToString(),
                                city = reader["city"].ToString(),
                                state = reader["state"].ToString(),
                                zip = reader["zip"].ToString()
                            });
                        }
                    }
                }

                document.Open();
                string body = string.Empty;
                using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/Receipt.html")))
                {
                    body = reader.ReadToEnd();
                }

                foreach (var item in list)
                {
                    body = body.Replace("[client_name]", item.firstname);
                    body = body.Replace("[client_addreess]", item.address_one);
                    body = body.Replace("[client_mobile]", item.report_desc);
                    body = body.Replace("[comp_name]", item.comp_name);
                    body = body.Replace("[user_added_by_role_name]", item.user_added_by_role_name);
                    body = body.Replace("[security_company_address_one]", item.security_company_addressone);
                    body = body.Replace("[security_company_mobile]", item.security_company_mobile);
                    body = body.Replace("[report_type]", item.report_type);
                    body = body.Replace("[address_one]", item.address_one);
                    body = body.Replace("[location_name]", item.location);
                    body = body.Replace("[user_added_by_firstname]", item.location);
                    body = body.Replace("[report_date]", item.location);
                    body = body.Replace("[city]", item.city);
                    body = body.Replace("[state]", item.state);
                    body = body.Replace("[zip]", item.zip);
                }
                var itemsTable = @"<table>";
                foreach (var item in list)
                    if (item != null)
                    {
                        // Each CheckBoxList item has a value of ITEMNAME|ITEM#|QTY, so we split on | and pull these values out...
                        var client_name = item.firstname;
                        var Report_type = item.report_type;
                        var location = item.location;
                        var user_creater = item.user_by;
                        var dscription = item.report_desc;
                        var date_report = item.report_date;
                        var line = "----------------------------------------------------------------";
                        if (Report_type == "DR")
                        {
                            itemsTable += string.Format("<tr><td>{0}</td><td>{1}</td></tr>" +
                             "<tr><td>{2}</td><td>{3}</td></tr>" +
                             "<tr><td>{4}</td><td>{5}</td></tr>" +
                             "<tr><td>{6}</td><td>{7}</td></tr>",
                                                     client_name, Report_type, location, user_creater, dscription, date_report, line, line);
                        }
                        else
                        {
                            itemsTable += string.Format("<tr><td>{0}</td><td bgcolor='#FF0000'>{1}</td></tr>" +
                           "<tr><td>{2}</td><td>{3}</td></tr>" +
                           "<tr><td>{4}</td><td>{5}</td></tr>" +
                           "<tr><td>{6}</td><td>{7}</td></tr>",
                                                   client_name, Report_type, location, user_creater, dscription, date_report, line, line);
                        }
                    }
                itemsTable += "</table>";
                body = body.Replace("[ITEMS]", itemsTable);


                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(body), null);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                document.Close();

                byte[] byteInfo = workStream.ToArray();
                string base64String = Convert.ToBase64String(byteInfo, 0, byteInfo.Length);
                //workStream.Write(byteInfo, 0, byteInfo.Length);
                //workStream.Position = 0;
               
                return Json(base64String);
            }
            catch (Exception ex)
            { 
                throw;
            }
        }


        [HttpGet("view_report_detail_indiv")]
        [ResponseCache(VaryByHeader = "view_report_detail_indiv", Duration = 100)]
        public JsonResult view_report_detail_indiv(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.view_report_detail_indiv(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        #region only for super admin
        [Authorize]
        [HttpGet("total_security_comp")]
        [ResponseCache(VaryByHeader = "total_security_comp", Duration = 100)]
        public JsonResult total_security_comp()
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.total_security_comp();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }
        #endregion

        #region only for super admin
        [Authorize]
        [HttpGet("sms_total_users")]
        [ResponseCache(VaryByHeader = "sms_total_users", Duration = 100)]
        public JsonResult sms_total_users()
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.sms_total_users();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }
        #endregion

        #region for all users
        [Authorize]
        [HttpGet("sms_total_report")]
        [ResponseCache(VaryByHeader = "sms_total_report", Duration = 100)]
        public JsonResult sms_total_report(Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.sms_total_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
        #endregion

        #region for all users
        [Authorize]
        [HttpPost("total_incident_report")]
        [ResponseCache(VaryByHeader = "total_incident_report", Duration = 100)]
        public JsonResult total_incident_report([FromBody]Report rpt)
        {
            try
            {
                ReportServices rps = new ReportServices(_config);
                var response = rps.sms_total_incident_report(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
        #endregion

        #region for only security company(dashboard)
        [Authorize]
        [HttpPost("total_client_manager_guard")]
        [ResponseCache(VaryByHeader = "total_client_manager_guard", Duration = 100)]
        public JsonResult Get_All_UserList([FromBody]Report rpt)
        {
            try
            {
                ReportServices src = new ReportServices(_config);
                var response = src.sms_total_client_manager_guard(rpt);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
        #endregion

        [Authorize]
        [HttpGet("client_location_based_services")]
        public JsonResult client_location_based_services(int client_id, int location_id)
        {
            try
            {
                ReportServices reg = new ReportServices(_config);
                var response = reg.sms_client_location_based_services(client_id, location_id);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}