﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using eix_tracker.Models;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;

namespace WebApiAuthDemo.Controllers
{
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<TokenController> _logger;
        public TokenController(IConfiguration configuration, ILogger<TokenController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("token")]
        public IActionResult Post([FromBody]Login loginViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = "";
                    LoginServices ls = new LoginServices(_configuration);
                    var datraasd = ls.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
                    if (Convert.ToString(datraasd.Tables[0].Rows[0]["messageCode"]) == "1")
                    {
                        userId = datraasd.Tables[0].Rows[0]["user_id"].ToString();
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, loginViewModel.username),
                             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };
                        var token = new JwtSecurityToken
                        (
                            issuer: _configuration["Issuer"],
                            audience: _configuration["Audience"],
                            claims: claims,
                            expires: DateTime.UtcNow.AddDays(60),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SigningKey"])),
                                 SecurityAlgorithms.HmacSha256)
                        );
                        var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);
                        //LoginServices ls = new LoginServices();
                        //var datra = ls.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
                        //datra.Tables[0].Columns.Add("access_token");
                        //datra.Tables[0].Rows[0].Columns["access_token"].
                        datraasd.Tables[0].Columns.Add("access_token");
                        datraasd.Tables[0].Rows[0]["access_token"] = encodedJwt;
                        //var response = new
                        //{
                        //    //access_token = encodedJwt,
                        //    //userID = userId,
                        //    datra
                        //};
                        return Json(datraasd);
                    }
                    else
                    {
                        userId = datraasd.Tables[0].Rows[0]["message"].ToString();
                        if (userId == "Login Failed")
                        {
                            //LoginServices lsds = new LoginServices();
                            //var data_failed = lsds.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
                            return Json(datraasd);
                        }
                    }

                    //var userId = GetUserIdFromCredentials(loginViewModel);
                    //token.Payload["userID"] = '1';
                    //if (userId == "Login Failed")
                    //{
                    //    LoginServices lsds = new LoginServices();
                    //    var datra = lsds.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
                    //    return Json(datra);
                    //}
                    //else
                    //{
                    //    if (userId == "-1")
                    //    {
                    //        return Unauthorized();
                    //        //return Json(userId);
                    //    }
                    //    var claims = new[]
                    //    {
                    //        new Claim(JwtRegisteredClaimNames.Sub, loginViewModel.username),
                    //         new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                    //    };
                    //    var token = new JwtSecurityToken
                    //    (
                    //        issuer: _configuration["Issuer"],
                    //        audience: _configuration["Audience"],
                    //        claims: claims,
                    //        expires: DateTime.UtcNow.AddDays(60),
                    //        notBefore: DateTime.UtcNow,
                    //        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SigningKey"])),
                    //             SecurityAlgorithms.HmacSha256)
                    //    );
                    //    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);
                    //    LoginServices ls = new LoginServices();
                    //    var datra = ls.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
                    //    //datra.Tables[0].Columns.Add("access_token");
                    //    //datra.Tables[0].Rows[0].Columns["access_token"].
                    //    datra.Tables[0].Columns.Add("access_token");
                    //    datra.Tables[0].Rows[0]["access_token"] = encodedJwt;
                    //    //var response = new
                    //    //{
                    //    //    //access_token = encodedJwt,
                    //    //    //userID = userId,
                    //    //    datra
                    //    //};
                    //    return Json(datra);
                    //    //return Ok(Json(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented })));
                    //    //return new JsonResult(response, new JsonSerializerSettings { Formatting = Formatting.Indented })
                    //    //{
                    //    //    StatusCode = 201, // Status code here 
                    //    //};
                    //    //var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

                    //    //var response = new
                    //    //{
                    //    //    //access_token = encodedJwt,
                    //    //    datra
                    //    //};

                    //    ////return Ok(Json(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented })));

                    //    //return new JsonResult(response)
                    //    //{
                    //    //    StatusCode = 201 // Status code here 
                    //    //};
                    //}
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        private string GetUserIdFromCredentials(Login loginViewModel)
        {
            string userId = "-1";
            LoginServices ls = new LoginServices(_configuration);
            var datra = ls.Getloginuser(loginViewModel.username, loginViewModel.password, loginViewModel.public_ip, loginViewModel.device_id, loginViewModel.browser_desc, loginViewModel.os_desc);
            if (Convert.ToString(datra.Tables[0].Rows[0]["messageCode"]) == "1")
            {
                userId = datra.Tables[0].Rows[0]["user_id"].ToString();
            }
            else
            {
                userId = datra.Tables[0].Rows[0]["message"].ToString();
            }
            return userId;
        }
    }
}