﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;

namespace eix_tracker.Controllers
{
    public class userController : Controller
    {
        public IActionResult Index()
         {
            MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(eix_tracker.Models.MusicStoreContext)) as MusicStoreContext;

            return View(context.Getusers());
        }
    }
}