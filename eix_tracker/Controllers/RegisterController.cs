﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Register")]
    public class RegisterController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<RegisterController> _logger;
        public RegisterController(IConfiguration config,ILogger<RegisterController> logger)
        {
            _logger = logger;
            _config = config;

        }

        //[Authorize]
        [HttpPost("registeruser")]
        public JsonResult post([FromBody]register user)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.RegisterUser(user);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("forgot_password")]
        public JsonResult Forgot_password(register rg)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.Forgot_password(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_view_profile")]
        //[ResponseCache(VaryByHeader = "user_view_profile", Duration = 100)]
        public JsonResult Get_user_view_profile(register rg)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.user_view_profile(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("delete_User_profile")]
        public JsonResult Delete_User_status([FromBody]register rg)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.Delete_User(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("user_verify")]
        public JsonResult Get_user_verify(string user_id,string user_key)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.user_Verify(user_id, user_key);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpPost("user_reset_password")]
        public JsonResult reset_user_password([FromBody]register rg)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.Reset_user(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("user_change_password")]
        public JsonResult change_user_password([FromBody]register rg)
        {
            try
            {
                RegisterServices sdf = new RegisterServices(_config);
                var response = sdf.Change_password(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("Update_user_Profile")]
        public JsonResult user_profile_update([FromBody]register rg)
        {
            try
            {
                RegisterServices reg = new RegisterServices(_config);
                var response = reg.Updateuser_Profile(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("Getall_users")]
        [ResponseCache(VaryByHeader = "Get_all_users", Duration = 100)]
        public JsonResult GetallUserList(register user)
        {
            try
            {
                RegisterServices rs = new RegisterServices(_config);
                var response = rs.Get_All_user(user);
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("check_email")]
        //[Consumes("application/x-www-form-urlencoded")]
        public JsonResult check_email([FromBody]string emailaddr)
        {
            try
            {
                RegisterServices rs = new RegisterServices(_config);
                var response = rs.sms_check_email(emailaddr);
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_update_status")]
        public JsonResult user_update_status(string userid,string change_status)
        {
            try
            {
                RegisterServices rs = new RegisterServices(_config);
                var response = rs.sms_user_update_status(userid, change_status);
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
