﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace eix_tracker.Services
{
    public class ClientServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public ClientServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region InsertUser_location
        public DataSet add_client_location(Client cl)
        {

            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            //string Myconectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_user_add_location", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@client_id", cl.client_id);
            cmd.Parameters.AddWithValue("@location_name", cl.location_name);
            cmd.Parameters.AddWithValue("@address_one", cl.address_one);
            cmd.Parameters.AddWithValue("@address_two", cl.address_two);
            cmd.Parameters.AddWithValue("@city", cl.city);
            cmd.Parameters.AddWithValue("@state", cl.state);
            cmd.Parameters.AddWithValue("@zip", cl.zip);
            cmd.Parameters.AddWithValue("@lat", cl.latitude);
            cmd.Parameters.AddWithValue("@longitude", cl.longitude);
            cmd.Parameters.AddWithValue("@location_status", cl.location_status);

            cmd.Parameters.AddWithValue("@qr_code", uniqueFileName + "." + "png");
            Byte[] bytes = Convert.FromBase64String(cl.QR_code);
            string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/upload/"));
            File.WriteAllBytes(fullFileName + uniqueFileName + "." + "png", bytes);


            cmd.Parameters.AddWithValue("@entry_date", cl.entry_date);
            cmd.Parameters.AddWithValue("@zone_id", cl.zone_id);
            cmd.Parameters.AddWithValue("@userid", cl.userid);
            cmd.Parameters.AddWithValue("@location_type", cl.location_type);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region client_based_location
        public DataSet client_based_location(Client cl)
        {

            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            //string Myconectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_client_based_location", con);
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet dt = new DataSet();
            cmd.Parameters.AddWithValue("@client_id", cl.client_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region client_total_location
        public DataSet client_total_location(Client cl)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_total_location", con);
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet dt = new DataSet();
            cmd.Parameters.AddWithValue("@userid", cl.user_id);
            cmd.Parameters.AddWithValue("@roleid", cl.role_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@total_location_count", MySqlDbType.VarChar, 500);
                cmd.Parameters["@total_location_count"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region View_user_Location
        public DataSet view_user_location(Client cl)
        {
            string Myconnectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection conn = new MySqlConnection(Myconnectionstring);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("sms_user_view_location", conn);
            cmd.Parameters.AddWithValue("@userid", cl.user_id);
            cmd.Parameters.AddWithValue("@roleid", cl.role_id);
            DataSet ds = new DataSet();
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(ds);
            }
            foreach (DataTable tables in ds.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }

            return ds;

        }
        #endregion

        #region user_Location_Details
        public DataSet user_Location_Details(Client cl)
        {

            string Myconnectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection conn = new MySqlConnection(Myconnectionstring);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("sms_location_detail", conn);
            cmd.Parameters.AddWithValue("@locationid", cl.location_id);
            DataSet ds = new DataSet();
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(ds);
            }
            foreach (DataTable tables in ds.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return ds;

        }
        #endregion

        #region update_User_location
        public DataSet update_user_location(Client cl)
        {
            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_user_update_location", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@locationid", cl.location_id);
            cmd.Parameters.AddWithValue("@location_name", cl.location_name);
            cmd.Parameters.AddWithValue("@address_one", cl.address_one);
            cmd.Parameters.AddWithValue("@address_two", cl.address_two);
            cmd.Parameters.AddWithValue("@city", cl.city);
            cmd.Parameters.AddWithValue("@state", cl.state);
            cmd.Parameters.AddWithValue("@zip", cl.zip);
            cmd.Parameters.AddWithValue("@lat", cl.latitude);
            cmd.Parameters.AddWithValue("@longitude", cl.longitude);

            cmd.Parameters.AddWithValue("@qr_code", uniqueFileName + "." + "png");
            Byte[] bytes = Convert.FromBase64String(cl.QR_code);
            string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/upload/"));
            File.WriteAllBytes(fullFileName + uniqueFileName + "." + "png", bytes);

            cmd.Parameters.AddWithValue("@location_status", cl.location_status);
            cmd.Parameters.AddWithValue("@update_date", cl.entry_date);
            cmd.Parameters.AddWithValue("@zone_id", cl.zone_id);
            //cmd.Parameters.AddWithValue("@clientid", cl.client_id);
            cmd.Parameters.AddWithValue("@location_type", cl.location_type);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;

        }
        #endregion

        #region Delete_User_location
        public DataSet Delete_User_location(Client cl)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_user_delete_location", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@locationId", cl.location_id);
            cmd.Parameters.AddWithValue("@update_date", cl.entry_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                adp.Fill(dt);

            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;

        }
        #endregion
    }
}
