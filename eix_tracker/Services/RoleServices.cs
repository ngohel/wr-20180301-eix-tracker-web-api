﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Data;
using Microsoft.Extensions.Logging;
using System.IO;
using Rotativa.AspNetCore;
using System.Drawing;
using Rotativa.AspNetCore.Options;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Services
{
    public class RoleServices
    {
        //public string ConnectionStrings { get; set; }
        private IConfiguration Configuration { get; set; }
        public RoleServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public MySqlConnection connection()
        {
            //string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            return new MySqlConnection(Myconectionstring);
        }

        #region GET_ALLROLES_by_list
        public IActionResult DemoModelPDF()
        {
            //string Myconectionstring = "server=localhost;database=trackerdb;user=root;password=";
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Role()
                        {
                            Role_Id = Convert.ToInt16(reader["Role_ID"]).ToString(),
                            Role_Name = reader["Role_Name"].ToString(),
                        });
                    }
                }
            }
            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/pdf/"), uniqueFileName + ".pdf");

            return new ViewAsPdf("DemoModelPDF")
            {
                FileName = uniqueFileName,
                PageSize = Rotativa.AspNetCore.Options.Size.A4,
                PageOrientation = Orientation.Portrait,
                PageMargins = { Left = 0, Right = 0 },
                SaveOnServerPath = filePath,
            };
            //return list;
        }
        #endregion

        #region GET_ALLROLES
        public DataSet GetRoles()
        {
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region GET_Role_List
        public DataSet Get_user_Role_List(Role rl)
        {
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role_list", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@role_id", rl.Role_Id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }

                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }

        }
        #endregion

        #region Role_Access
        public DataSet role_access(Role rl)
        {
            //string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role_access", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@accessid", rl.access_ID);
                    cmd.Parameters.AddWithValue("@role_id", rl.Role_Id);
                    cmd.Parameters.AddWithValue("@roleadd", rl.role_add);
                    cmd.Parameters.AddWithValue("@roleupdate", rl.role_update);
                    cmd.Parameters.AddWithValue("@roledelete", rl.role_delete);
                    cmd.Parameters.AddWithValue("@roleview", rl.role_view);
                    cmd.Parameters.AddWithValue("@update_date", rl.Update_Date);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                    cmd.ExecuteNonQuery();
                }

                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

    }







}
