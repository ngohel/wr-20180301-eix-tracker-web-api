﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;
namespace eix_tracker.Services
{
    public class ZoneServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public ZoneServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region InsertZone
        public DataSet add_route_zone(Zone zn)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_add_route_zone", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@zone_name", zn.Zone_Name);
            cmd.Parameters.AddWithValue("@userid", zn.user_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region Get_all_zone
        public DataSet view_route_Zone(Zone zn)
        {
            string MyconnectionMyconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(MyconnectionMyconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_view_route_zone", conn1);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", zn.user_id);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Delete_root_zone
        public DataSet delete_route_zone(Zone zn)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_delete_route_zone", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@zoneid", zn.zoneid);
            cmd.Parameters.AddWithValue("@update_date", zn.update_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion
    }
}
