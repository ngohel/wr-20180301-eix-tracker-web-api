﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace eix_tracker.Models
{
    public class LoginServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public LoginServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetUserlogins
        public DataSet Getloginuser(
            string uname,
            string pswd,
            string publicip,
            string deviceid,
            string browserdesc,
            string osdesc)
        {
            string user = string.Empty;
            string msg = string.Empty;
            string gt_id = string.Empty;
            string role_id = string.Empty;
            string login_attempt_failed = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd1 = new MySqlCommand("sms_user_profile_login", con);
            DataSet dt = new DataSet();
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@emailaddr", uname);
            cmd1.Parameters.AddWithValue("@password_p", pswd);
            cmd1.Parameters.AddWithValue("@public_ip", publicip);
            cmd1.Parameters.AddWithValue("@device_id", deviceid);
            cmd1.Parameters.AddWithValue("@browser_desc", browserdesc);
            cmd1.Parameters.AddWithValue("@os_desc", osdesc);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
            {
                cmd1.Parameters.Add("@get_user_id", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_user_id"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_role_id", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_role_id"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@loginattempt", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@loginattempt"].Direction = ParameterDirection.Output;
                //cmd1.ExecuteNonQuery();
                adp.Fill(dt);
                con.Close();
                gt_id = (string)cmd1.Parameters["@get_user_id"].Value.ToString();
                role_id = (string)cmd1.Parameters["@get_role_id"].Value.ToString();
                user = (string)cmd1.Parameters["@checkUser"].Value.ToString();
                msg = (string)cmd1.Parameters["@checkMessage"].Value.ToString();
                if (user == "0")
                {
                    login_attempt_failed = (string)cmd1.Parameters["@loginattempt"].Value.ToString();
                }
            }
            foreach (DataTable table in dt.Tables)
            {
                table.TableName = table.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region Login Attempt
        public DataSet Login_Attempt(Login_Attempt lga)
        {
            string user = string.Empty;
            string UserCode = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_check_login_attempt", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@emailaddr", lga.email_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@random_num"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
                user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                UserCode = (string)cmd.Parameters["@random_num"].Value.ToString();
                if (user == "1")
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        SmtpClient SmtpServer = new SmtpClient();
                        SmtpServer.UseDefaultCredentials = true;
                        mail.To.Add(lga.email_id);
                        mail.From = new MailAddress("tapan.bhatt@eixsys.com");
                        mail.Subject = "Your one-time passcode to confirm password reset";
                        mail.IsBodyHtml = true;
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", lga.email_id + ",");
                        var message_title = "You attempted incorrect password many times. Please use pass-code.";
                        var message_header = "Pass-Code";
                        body = body.Replace("{header}", message_header);
                        body = body.Replace("{message_title_html}", message_title);
                        body = body.Replace("{Title}", UserCode);
                        //string body = "Hello, " + rg.Email_ID;
                        //body += "<br /><br />use the passcode to register";
                        //body += "<br /><h1>" + message_code + "</h1>";
                        //body += "<br /><br />Thanks";
                        mail.Body = body;
                        SmtpServer.Host = "smtp.office365.com";
                        SmtpServer.Port = 587;
                        SmtpServer.Credentials = new NetworkCredential("tapan.bhatt@eixsys.com", "Tapan172");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        try
                        {
                            SmtpServer.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                }
                con.Close();
            }
            foreach (DataTable table in dt.Tables)
            {
                table.TableName = table.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        public Login Compareuser(string username, string password_p)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<Login> list = new List<Login>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_profile_login", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Login()
                        {
                            username = username,
                            password = password_p,
                        });
                    }
                }
            }
            return null;
        }
        public List<Login> Userloginvalues()
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");

            List<Login> list = new List<Login>();


            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_profile_login", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Login()
                        {
                            username = reader["username"].ToString(),
                            password = reader["password_p"].ToString(),
                        });
                    }
                }
            }
            return list;
        }
    }

}

