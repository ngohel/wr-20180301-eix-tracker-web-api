﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace eix_tracker.Services
{
    public class taskservices
    {

        #region Config_services
        private IConfiguration Configuration { get; set; }
        public taskservices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region ADD_USER_TASK
        public DataSet add_user_task(TaskManage tm)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_add_task", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userid", tm.userid);
            cmd.Parameters.AddWithValue("@clientid", tm.clientid);
            cmd.Parameters.AddWithValue("@locationid", tm.locationid);
            cmd.Parameters.AddWithValue("@question_id", tm.questionid);
            cmd.Parameters.AddWithValue("@service_id", tm.serviceId);
            cmd.Parameters.AddWithValue("@task_name", tm.Task_Name);
            cmd.Parameters.AddWithValue("@task_description", tm.Description);
            cmd.Parameters.AddWithValue("@task_status", tm.task_status);
            cmd.Parameters.AddWithValue("@task_frequency", tm.task_frequency);
            cmd.Parameters.AddWithValue("@task_assigment", tm.Task_Assigment);
            cmd.Parameters.AddWithValue("@entry_date", tm.entry_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);

            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region View_user_task
        public DataSet View_user_task(TaskManage tm)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_view_task", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userid", tm.userid);
            cmd.Parameters.AddWithValue("@roleid", tm.roleid);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;

        }
        #endregion

        #region View_task_details
        public DataSet View_task_details(TaskManage tm)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_task_detail", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region update_user_task
        public DataSet update_user_task(TaskManage tm)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_update_task", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            cmd.Parameters.AddWithValue("@task_name", tm.Task_Name);
            cmd.Parameters.AddWithValue("@task_description", tm.Description);
            cmd.Parameters.AddWithValue("@task_frequency", tm.task_frequency);
            cmd.Parameters.AddWithValue("@task_assigment", tm.Task_Assigment);
            cmd.Parameters.AddWithValue("@update_date", tm.entry_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region delete_task_details
        public DataSet delete_task_details(TaskManage tm)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_delete_task", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            cmd.Parameters.AddWithValue("@update_date", tm.entry_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_location_based_services
        public DataSet sms_location_based_services(int location_id)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_location_based_services", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@location_id", location_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_services_based_questions
        public DataSet sms_services_based_questions(int service_id)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_services_based_questions", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@service_id", service_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_view_services_detail
        public DataSet sms_view_services_detail(int servicesid)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_view_services_detail", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@servicesid", servicesid);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                //cmd.ExecuteNonQuery();
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion
    }
}
