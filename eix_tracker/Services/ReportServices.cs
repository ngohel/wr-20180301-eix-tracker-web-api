﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Rotativa.AspNetCore;
using System.IO;
using Rotativa.AspNetCore.Options;
using MySql.Data.MySqlClient;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Services
{
    public class ReportServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public ReportServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region connection
        public MySqlConnection connection()
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            return new MySqlConnection(Myconectionstring);
        }
        #endregion

        #region Insert_Report
        public DataSet Insert_Report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_add_report", conn);
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@client_id", rpt.clientid);
                cmd.Parameters.AddWithValue("@location_id", rpt.locationid);
                cmd.Parameters.AddWithValue("@entry_date", rpt.entry_date);
                cmd.Parameters.AddWithValue("@report_type", rpt.report_type);
                cmd.Parameters.AddWithValue("@report_desc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@service_id", rpt.service_id);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@report_detail_id", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@report_detail_id"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Insert_Report_detail
        public DataSet Insert_Report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_add_report_detail", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.Parameters.AddWithValue("@report_desc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@entry_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@report_detail_id", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@report_detail_id"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_view_report
        public DataSet user_view_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_report", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region view_incident_report
        public DataSet view_incident_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_incident_report", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region view_report_detail_indiv
        public DataSet view_report_detail_indiv(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_report_detail_indiv", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@report_detail_id", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_report_detail
        public DataSet user_report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_report_detail", conn);
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();

                }
                return ds;
            }
        }
        #endregion



        //[HttpGet("get_report_detail_pdf")]
        //public FileStreamResult pdf(Report rg)
        //{
        //    MemoryStream workStream = new MemoryStream();
        //    Document document = new Document();
        //    PdfWriter.GetInstance(document, workStream).CloseStream = false;


        //    List<Report> list = new List<Report>(); 
        //    using (MySqlConnection conn = connection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("sms_view_report_detail", conn);
        //        cmd.Parameters.AddWithValue("@reportid", rg.report_id);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
        //        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
        //        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                list.Add(new Report()
        //                {
        //                    report_id = Convert.ToInt16(reader["report_id"]),
        //                });
        //            }
        //        }
        //    }
        //    document.Open();
        //    string body = string.Empty;
        //    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/Receipt.html")))
        //    {
        //        body = reader.ReadToEnd();
        //    }

        //    //foreach (var item in list)
        //    //{
        //    //    body = body.Replace("[ORDERID]", item.Role_Id);
        //    //    body = body.Replace("[TOTALPRICE]", item.Role_Name);
        //    //    body = body.Replace("[ORDERDATE]", DateTime.Now.ToString());
        //    //}
        //    var itemsTable = @"<table><tr><th style=""font-weight: bold"">Item #</th></tr>";
        //    foreach (var item in list)
        //        if (item != null)
        //        {
        //            // Each CheckBoxList item has a value of ITEMNAME|ITEM#|QTY, so we split on | and pull these values out...
        //            var pieces = item.report_desc;
        //            itemsTable += string.Format("<tr><td>{0}</td></tr>",
        //                                        pieces);
        //        }
        //    itemsTable += "</table>";
        //    body = body.Replace("[ITEMS]", itemsTable);


        //    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(body), null);
        //    foreach (var htmlElement in parsedHtmlElements)
        //        document.Add(htmlElement as IElement);
        //    document.Close();
        //    byte[] byteInfo = workStream.ToArray();
        //    workStream.Write(byteInfo, 0, byteInfo.Length);
        //    workStream.Position = 0;
        //    return new FileStreamResult(workStream, "application/pdf");
        //}





        #region user_delete_report
        public DataSet user_delete_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_delete_report", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_delete_report_detail
        public DataSet user_delete_report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_delete_report_detail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportdetailid", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_report_update
        public DataSet user_report_update(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_update_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@reportdetailid", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@reportdesc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region total_security_comp
        public DataSet total_security_comp()
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_security_comp", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_report
        public DataSet sms_total_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_incident_report
        public DataSet sms_total_incident_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_incident_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_users
        public DataSet sms_total_users()
        {
            try
            {
                using (MySqlConnection conn = connection())
                {
                    string user = string.Empty;
                    MySqlCommand cmd = new MySqlCommand("sms_total_users", conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                        //user = (string)cmd.Parameters["@random_num"].Value.ToString();
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        #endregion

        #region sms_total_client_manager_guard
        public DataSet sms_total_client_manager_guard(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_client_manager_guard", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_client_location_based_services
        public DataSet sms_client_location_based_services(int client_id, int location_id)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_client_location_based_services", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_id", client_id);
                    cmd.Parameters.AddWithValue("@location_id", location_id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                    //cmd.ExecuteNonQuery();
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion
    }
}
