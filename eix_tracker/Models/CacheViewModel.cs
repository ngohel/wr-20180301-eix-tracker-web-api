﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class CacheViewModel
    {
        public DateTime? FirstTime { get; set; }
        public DateTime SecondTime { get; set; }
    }
}
