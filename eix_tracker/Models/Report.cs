﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class Report
    {
        public string report_type { get; set; }
        public string entry_date { get; set; }
        public string report_desc { get; set; }
        //public int Location_id { get; set; }
        public string service_id { get; set; }
        public string report_detail_id { get; set; }
        public int user_id { get; set; }
        public int locationid { get; set; }
        public int report_id { get; set; }
        public int roleid { get; set; }
        public int clientid { get; set; }  
    }

    public class Report_detail_pdf
    {
        public int report_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address_one { get; set; }
        public string address_two { get; set; }
        public string report_desc { get; set; }
        public string comp_name { get; set; }
        public string report_type { get; set; }
        public string user_added_by_role_name { get; set; }
        public string security_company_addressone { get; set; }
        public string security_company_mobile { get; set; }
        public string location { get; set; }
        public string user_by { get; set; }
        public string report_date { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
    }
}
