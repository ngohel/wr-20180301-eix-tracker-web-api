﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class attachmentCollection
    {  
        public List<attachment> arrAttachment { get; set; }
    }
    public class attachment
    {
        public int report_detail_id { get; set; }
        public string ext { get; set; }
        public string attachment_type { get; set; }
        public string attachment_name { get; set; }
        public DateTime entry_date { get; set; }
    }
}
