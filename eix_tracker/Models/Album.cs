﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace eix_tracker.Models
{
    public class Album
    {
        private MusicStoreContext context;

        public int a_id {get; set;}
        public string Name { get; set; }
        public string ArtistName { get; set; }
        public string Price { get; set; }
        public string Mvsongs { get; set; }
    }
   
}
