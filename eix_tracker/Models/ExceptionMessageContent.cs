﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class ExceptionMessageContent
    {
        public string Error { get; set; }
        public string Message { get; set; }
    }
}
