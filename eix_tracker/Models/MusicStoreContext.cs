﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using eix_tracker.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System.Data;

namespace eix_tracker.Models
{
    public class MusicStoreContext
    {
        #region Connection_Settings
        public string ConnectionStrings { get; set; }

        public MusicStoreContext(string connectionString)
        {
            this.ConnectionStrings = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionStrings);
        }
        #endregion

        #region GetAlbums
        public List<Album> GetAllAlbums()
        {
            List<Album> list = new List<Album>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from album", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Album()
                        {
                            a_id = Convert.ToInt32(reader["a_id"]),
                            Name = reader["Name"].ToString(),
                            ArtistName = reader["ArtistName"].ToString(),
                            Price = reader["Price"].ToString(),
                            Mvsongs = reader["Mvsongs"].ToString()
                        });
                    }
                }
            }
            return list;
        }
        #endregion

        #region GetUser
        public List<Login> Getusers()
        {
            //List<Login> list = new List<Login>();

            //using (MySqlConnection conn = GetConnection())
            //{
            //    conn.Open();
            //    MySqlCommand cmd = new MySqlCommand("sms_user_profile_test", conn);

            //    cmd.CommandType = CommandType.StoredProcedure;

            //    using (var reader = cmd.ExecuteReader())
            //    {
            //        while (reader.Read())
            //        {
            //            list.Add(new Login()
            //            {
            //                username = reader["user_name"].ToString(),
            //                password = reader["password_has"].ToString(),
            //            });
            //        }
            //    }
            //}
            return null;
        }
        #endregion


        //public Login Compareuser(string username, string password)
        //{
        //    string Myconectionstring = "server=mysql.eixtracker.com;database=eixtrackerdb;user=eixtrackeruser;password=tracker123#";

        //    List<Login> list = new List<Login>();


        //    using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("sms_user_profile_login", conn);

        //        cmd.CommandType = CommandType.StoredProcedure;

        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                list.Add(new Login()
        //                {
        //                    username = reader["user_name"].ToString(),
        //                    password = reader["password_has"].ToString(),
        //                });
        //            }
        //        }
        //    }
        //    return null;
        //}


    }

}

